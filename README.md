# Instahelper

## Код инфраструктуры для шаблонного [микросервиса](https://gitlab.com/svkoval67/skillbox-diploma).

Командой `make build_image` подготавливаем образ в YC, который будет использован для развертывания серверов. Файл `.env.template` переименовываем в `.env`. Переменная `API_TOKEN` задается в репозитории [микросервиса](https://gitlab.com/svkoval67/skillbox-diploma/-/settings/access_tokens). Инициализируем остальные переменные в `terraform/.keys.auto.tfvars` и `ansible/.vars.local`.
- [ ] `ci_token` задается при создании runner в Gitlab
- [ ] `api_token` тот же самый, что `API_TOKEN` в файле `.env`
- [ ] `oauth` OAuth-токен для доступа к облаку YC
- [ ] `S3_AccessKey` и `S3_SecretKey` создаются в YC командой ```yc iam access-key create --service-account-name srvact```
- [ ] `monitoring_api_key` создается командой ```yc iam api-key create --service-account-name srvacct --description "this API-key is for monitoring"```

# Перенаправление портов для тестирования/отладки

- [ ] Grafana `ssh -f -N ci.tripleap.ru -L 3000:ci.tripleap.ru:3000`
- [ ] Victoria Metrics `ssh -f -N ci.tripleap.ru -L 8428:ci.tripleap.ru:8428`
- [ ] Consul `ssh -f -N ci.tripleap.ru -L 8500:consul.tripleap.ru:8500`
- [ ] kibana `ssh -f -N ci.tripleap.ru -L 5601:kibana.tripleap.ru:5601`

# Тестирование consul
```
curl -X PUT -d 0b2cf90e http://consul.tripleap.ru:8500/v1/kv/main/release_version
curl -X PUT -d 83188f20 http://consul.tripleap.ru:8500/v1/kv/develop/release_version

curl -X PUT -d @payload.json localhost:8500/v1/agent/service/register
cat payload.json
{
  "ID": "id03",
  "Name": "app",
  "Tags": ["app", "dev", "v1"],
  "Address": "192.168.10.21",
  "Port": 8080,
  "Checks": [{"Name": "chk", "Interval": 5, "HTTP": "http://192.168.10.21:8080/health"}],
  "EnableTagOverride": false
}
curl -H "Content-Type: application/json" -X PUT -d '{"ID": "id01","Name": "app","Tags": ["app", "prod", "v1"],"Address": "192.168.10.24","Port": 8080,"Checks": [{"Name": "chk", "Interval": 5, "HTTP": "http://192.168.10.24:8080/health"}]}' http://consul.tripleap.ru:8500/v1/agent/service/register
```

# Полезные ссылки

- [ ] https://github.com/terraform-yc-modules/terraform-yc-alb
- [ ] https://habr.com/ru/companies/nixys/articles/721404/
- [ ] https://docs.gitlab.com/runner/configuration/speed_up_job_execution.html
