terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
}

resource "yandex_compute_instance_group" "ig" {
  for_each    = var.group_options
  folder_id          = var.YC_KEYS.folder_id
  service_account_id = var.YC_KEYS.service_account_id

  instance_template {
    name = "${var.group_name}{instance.index}"
    platform_id = "standard-v3"
    resources {
      memory        = lookup(each.value, "memory", 2)
      cores         = lookup(each.value, "cores", 2)
      core_fraction = lookup(each.value, "core_fraction", 20)
    }

    scheduling_policy {
      preemptible = true
    }

    boot_disk {
      mode = "READ_WRITE"
      initialize_params {
        image_id = var.image_id
        size     = lookup(each.value, "boot_disk_size", 20)
      }
    }

    network_interface {
      network_id         = var.network_id
      subnet_ids         = ["${var.vpc_subnet1}", "${var.vpc_subnet2}", "${var.vpc_subnet3}"]
      security_group_ids = [var.ig-sg_id]
    }

    labels = {
      group = var.group_name
    }

    metadata = {
      ssh-keys = "ubuntu:${file("~/.ssh/id_ed25519.pub")}"
    }
  }

  scale_policy {
    dynamic "fixed_scale" {
      for_each = flatten([lookup(each.value, "fixed_scale", can(each.value["auto_scale"]) ? [] : [{ size = 1 }])])
      content {
        size = fixed_scale.value.size
      }
    }
    dynamic "auto_scale" {
      for_each = flatten([lookup(each.value, "auto_scale", [])])
      content {
        initial_size           = auto_scale.value.initial_size
        measurement_duration   = auto_scale.value.measurement_duration
        stabilization_duration = auto_scale.value.stabilization_duration
        cpu_utilization_target = auto_scale.value.cpu_utilization_target
      }
    }
  }

  allocation_policy {
    zones = each.value["zones"]
  }

  deploy_policy {
    max_unavailable = 1
    max_expansion   = 0
  }

  application_load_balancer {
  }
}