variable "YC_KEYS" {
  type = object({
    folder_id          = string
    service_account_id = string
  })
  description = "ID облака YC и сервисного аккаунта"
}

variable "image_id" {
  type        = string
  description = "ID дистрибутива для прикладных серверов"
}

variable "ig-sg_id" {}

variable "network_id" {
  
}

variable "vpc_subnet1" {
  
}

variable "vpc_subnet2" {
  
}

variable "vpc_subnet3" {
  
}

variable "group_name" {}

variable "group_options" {
  type = any
}