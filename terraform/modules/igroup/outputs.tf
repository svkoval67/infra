output "alb_tg_id" {
  value = yandex_compute_instance_group.ig["one"].application_load_balancer.0.target_group_id
}

output "servers" {
  value = yandex_compute_instance_group.ig["one"].instances[*].network_interface[0].ip_address
}

output "ip" {
  value = yandex_compute_instance_group.ig["one"].instances[0].network_interface[0].ip_address
}
