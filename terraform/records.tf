locals {
  certs_names = ["${var.site_name}", "dev.${var.site_name}", "monitoring.${var.site_name}", "sonarqube.${var.site_name}"]
}

resource "yandex_dns_zone" "external_zone" {
  name        = "externalzone"
  description = "externalzone"
  zone        = "${var.site_name}."
  public      = true
}

resource "yandex_cm_certificate" "lecerts" {
  count = length(local.certs_names)
  name  = "lecerts"

  self_managed {
    certificate = file("ssl/${count.index}all${var.certificate_fname}")
    private_key = file("ssl/${count.index}all${var.private_key_fname}")
  }
}

# resource "yandex_cm_certificate" "lecerts" {
#   count = "${length(local.certs_names)}"
#   name    = "lecerts"
#   domains = sort(["${local.certs_names[count.index]}", "www.${var.site_name}"])

#   managed {
#     challenge_type = "DNS_CNAME"
#     challenge_count = 2 # for each domain
#   }
# }

# data "yandex_cm_certificate" "lecerts" {
#   count = "${length(local.certs_names)}"
#   depends_on      = [yandex_dns_recordset.rs_lecerts]
#   certificate_id  = yandex_cm_certificate.lecerts[count.index].id
#   wait_validation = true
# }

resource "yandex_dns_recordset" "rs1" {
  zone_id = yandex_dns_zone.external_zone.id
  name    = "@"
  type    = "CNAME"
  ttl     = 200
  data    = ["${yandex_dns_recordset.rs2.name}"]
}

resource "yandex_dns_recordset" "rs2" {
  zone_id = yandex_dns_zone.external_zone.id
  name    = "www.${var.site_name}."
  type    = "A"
  ttl     = 200
  data    = [yandex_alb_load_balancer.alb.listener[0].endpoint[0].address[0].external_ipv4_address[0].address]
}

resource "yandex_dns_recordset" "rs3" {
  zone_id = yandex_dns_zone.external_zone.id
  name    = "dev.${var.site_name}."
  type    = "CNAME"
  ttl     = 200
  data    = ["${yandex_dns_recordset.rs2.name}"]
}

resource "yandex_dns_recordset" "rs4" {
  zone_id = yandex_dns_zone.external_zone.id
  name    = "adm.${var.site_name}."
  type    = "A"
  ttl     = 200
  data    = [yandex_compute_instance.vm-1.network_interface.0.nat_ip_address]
}

resource "yandex_dns_recordset" "rs5" {
  zone_id = yandex_dns_zone.external_zone.id
  name    = "ci.${var.site_name}."
  type    = "A"
  ttl     = 200
  data    = [yandex_compute_instance.vm-1.network_interface.0.ip_address]
}

resource "yandex_dns_recordset" "rs6" {
  zone_id = yandex_dns_zone.external_zone.id
  name    = "consul1.${var.site_name}."
  type    = "A"
  ttl     = 200
  data    = [module.consul[0].ip]
}

resource "yandex_dns_recordset" "rs7" {
  zone_id = yandex_dns_zone.external_zone.id
  name    = "consul.${var.site_name}."
  type    = "A"
  ttl     = 200
  data    = module.consul[0].servers
}

resource "yandex_dns_recordset" "rs8" {
  zone_id = yandex_dns_zone.external_zone.id
  name    = "monitoring.${var.site_name}."
  type    = "CNAME"
  ttl     = 200
  data    = ["${yandex_dns_recordset.rs2.name}"]
}

resource "yandex_dns_recordset" "rs9" {
  zone_id = yandex_dns_zone.external_zone.id
  name    = "kibana.${var.site_name}."
  type    = "A"
  ttl     = 200
  data    = [yandex_compute_instance.vm-2.network_interface.0.ip_address]
}

resource "yandex_dns_recordset" "rs10" {
  zone_id = yandex_dns_zone.external_zone.id
  name    = "logstash.${var.site_name}."
  type    = "CNAME"
  ttl     = 200
  data    = ["${yandex_dns_recordset.rs9.name}"]
}

resource "yandex_dns_recordset" "rs11" {
  zone_id = yandex_dns_zone.external_zone.id
  name    = "sonarqube.${var.site_name}."
  type    = "CNAME"
  ttl     = 200
  data    = ["${yandex_dns_recordset.rs2.name}"]
}

# эти записи нужны для проверки владения доменами при создании сертификата
# resource "yandex_dns_recordset" "rs_lecerts" {
#   count   = "${length(local.certs_names)}"
#   zone_id = yandex_dns_zone.external_zone.id
#   name    = yandex_cm_certificate.lecerts[count.index].challenges[0].dns_name
#   type    = yandex_cm_certificate.lecerts[count.index].challenges[0].dns_type
#   data    = [yandex_cm_certificate.lecerts[count.index].challenges[0].dns_value]
#   ttl     = 60
# }
