# разворачиваем окружение в YC
terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  backend "http" {}
  required_version = ">= 0.13"
}

data "terraform_remote_state" "state" {
  backend = "http"
  config = {
    address        = "https://gitlab.com/api/v4/projects/${var.PROJECT_ID}/terraform/state/${var.TF_STATE}"
    lock_address   = "https://gitlab.com/api/v4/projects/${var.PROJECT_ID}/terraform/state/${var.TF_STATE}/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/${var.PROJECT_ID}/terraform/state/${var.TF_STATE}/lock"
    username       = "${var.USER_BACKEND_LOGIN}"
    password       = "${var.USER_BACKEND_PASS}"
    lock_method    = "POST"
    unlock_method  = "DELETE"
    retry_wait_min = "5"
  }
}

provider "yandex" {
  zone      = "ru-central1-a"
  folder_id = var.YC_KEYS.folder_id
}