output "ci_server" {
  value = yandex_compute_instance.vm-1.network_interface.0.ip_address
}

output "prod_servers" {
  value = module.app_srvrs[0].servers
}

output "dev_servers" {
  value = module.app_srvrs[1].servers
}

output "consul_ip" {
  value = module.consul[0].ip
}

data "yandex_cm_certificate_content" "lecerts" {
  count          = length(local.certs_names)
  certificate_id = yandex_cm_certificate.lecerts[count.index].id
}

resource "local_file" "certificates" {
  count    = length(local.certs_names)
  content  = data.yandex_cm_certificate_content.lecerts[count.index].certificates[0]
  filename = "${count.index}all${var.certificate_fname}"
}

resource "local_file" "private_keys" {
  count    = length(local.certs_names)
  content  = data.yandex_cm_certificate_content.lecerts[count.index].private_key
  filename = "${count.index}all${var.private_key_fname}"
}
