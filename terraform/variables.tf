variable "YC_KEYS" {
  type = object({
    folder_id          = string
    service_account_id = string
  })
  description = "ID облака и сервисного аккаунта YC"
}

variable "TF_STATE" {
  type = string
  description = "Имя файла состояния terraform в Gitlab"
}

variable "PROJECT_ID" {
  type = string
  description = "This Gitlab`s project id"
}

variable "USER_BACKEND_LOGIN" {
  type = string
  description = "Gitlab project owner user name"
}

variable "USER_BACKEND_PASS" {
  type        = string
  description = "Access Token from Settings -> Access Tokens (scope: api, role: maintainer)"
}

variable "image_id" {
  type        = string
  description = "ID дистрибутива для серверов Consul"
}

variable "adminwks_image_id" {
  type        = string
  description = "ID дистрибутива управляющего компьютера (bastion, gitlab agent)"
}

variable "appsrv_image_id" {
  type        = string
  description = "ID дистрибутива для серверов приложения"
}

variable "site_name" {
  type        = string
  description = "DNS-имя сайта"
}

variable "certificate_fname" {
  type        = string
  description = "Certificate file name"
}

variable "private_key_fname" {
  type        = string
  description = "Private key file name"
}

variable "group_names" {
  type = list(string)
  description = "Метки для группировки серверов в yacloud inventory"
}
