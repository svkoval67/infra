locals {
  backend_defaults = {
    healthcheck = {
      timeout  = "10s"
      interval = "2s"
    }
    http_healthcheck = {
      path = "/"
    }
  }

  alb_load_balancer = {
    alb_backend_groups = {
      "prod" = {
        http_backends = [{
          name                       = "prod"
          port                       = 8080
          existing_target_groups_ids = [module.app_srvrs[0].alb_tg_id]
          healthcheck = {
            healthcheck_port = 8080
            http_healthcheck = {
              path = "/health"
            }
          }
        }]
      },
      "dev" = {
        http_backends = [{
          name = "dev"
          port = 8080
          healthcheck = {
            healthcheck_port = 8080
            http_healthcheck = {
              path = "/health"
            }
          }
          existing_target_groups_ids = [module.app_srvrs[1].alb_tg_id]
        }]
      },
      "grafana" = {
        http_backends = [{
          name = "grafana"
          port = 3000
          healthcheck = {
            healthcheck_port = 3000
            http_healthcheck = {
              path = "/api/health"
            }
          }
          existing_target_groups_ids = [yandex_alb_target_group.monitoring.id]
        }]
      },
      "victoria" = {
        http_backends = [{
          name = "victoria"
          port = 8428
          healthcheck = {
            healthcheck_port = 8428
            http_healthcheck = {
              path = "/metrics"
            }
          }
          existing_target_groups_ids = [yandex_alb_target_group.monitoring.id]
        }]
      },
      "sonarqube" = {
        http_backends = [{
          name = "sonarqube"
          port = 9000
          healthcheck = {
            healthcheck_port = 9000
            http_healthcheck = {
              path = "/api/system/status"
            }
          }
          existing_target_groups_ids = [yandex_alb_target_group.sonarqube.id]
        }]
      }
    },
    alb_virtual_hosts = {
      "virtual-router-1" = {
        http_router_name = "http-router-1"
        authority        = [var.site_name, "www.${var.site_name}"]
        route = {
          name = "http-virtual-route"
          http_route = {
            http_route_action = {
              backend_group_name = "prod"
            }
          }
        }
      },
      "virtual-router-2" = {
        http_router_name = "http-router-2"
        authority        = ["dev.${var.site_name}"]
        route = {
          name = "http-virtual-route"
          http_route = {
            http_route_action = {
              backend_group_name = "dev"
            }
          }
        }
      },
      "virtual-router-3" = {
        http_router_name = "http-router-3"
        authority        = ["monitoring.${var.site_name}"]
        route = [{
          name = "http-virtual-route1"
          http_route = {
            http_match = {
              path = {
                prefix = "/vict/"
              }
            }
            http_route_action = {
              backend_group_name = "victoria"
              prefix_rewrite     = "/"
            }
          }
          },
          {
            name = "http-virtual-route2"
            http_route = {
              http_route_action = {
                backend_group_name = "grafana"
              }
            }
        }]
      },
      "virtual-router-4" = {
        http_router_name = "http-router-4"
        authority        = ["sonarqube.${var.site_name}"]
        route = {
          name = "http-virtual-route"
          http_route = {
            http_route_action = {
              backend_group_name = "sonarqube"
            }
          }
        }
      }
    }
  }

  inbound_ports = [80, 443, 30080]
}

# из-за ограничения в две сети на облако, предварительно импортируем имеющуюся сеть
# terraform import yandex_vpc_network.network1 network1_id
resource "yandex_vpc_network" "network1" {
  name = "network1"
}

resource "yandex_vpc_subnet" "subnet-1" {
  name           = "subnet1"
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.network1.id
  v4_cidr_blocks = ["192.168.10.0/24"]
  route_table_id = yandex_vpc_route_table.rt.id
}

resource "yandex_vpc_subnet" "subnet-2" {
  name           = "subnet2"
  zone           = "ru-central1-b"
  network_id     = yandex_vpc_network.network1.id
  v4_cidr_blocks = ["192.168.20.0/24"]
  route_table_id = yandex_vpc_route_table.rt.id
}

resource "yandex_vpc_subnet" "subnet-3" {
  name           = "subnet3"
  zone           = "ru-central1-d"
  network_id     = yandex_vpc_network.network1.id
  v4_cidr_blocks = ["192.168.30.0/24"]
  route_table_id = yandex_vpc_route_table.rt.id
}

resource "yandex_vpc_gateway" "nat_gateway" {
  name = "nat-gateway"
  shared_egress_gateway {}
}

resource "yandex_vpc_route_table" "rt" {
  name       = "route-to-internet"
  network_id = yandex_vpc_network.network1.id

  static_route {
    destination_prefix = "0.0.0.0/0"
    gateway_id         = yandex_vpc_gateway.nat_gateway.id
  }
}

# Создание статического публичного IP-адреса для L7-балансировщика
# resource "yandex_vpc_address" "stat_address" {
#   name = "alb-static-address"
#   external_ipv4_address {
#     zone_id = "ru-central1-a"
#   }
# }

resource "yandex_vpc_security_group" "alb-sg" {
  name       = "alb-sg"
  network_id = yandex_vpc_network.network1.id

  egress {
    protocol       = "ANY"
    description    = "any"
    v4_cidr_blocks = ["0.0.0.0/0"]
    from_port      = 1
    to_port        = 65535
  }

  dynamic "ingress" {
    for_each = local.inbound_ports
    content {
      port           = ingress.value
      protocol       = "tcp"
      v4_cidr_blocks = ["0.0.0.0/0"]
    }
  }
}

resource "yandex_vpc_security_group" "ig-sg" {
  name       = "ig-sg"
  network_id = yandex_vpc_network.network1.id

  egress {
    protocol       = "ANY"
    description    = "any"
    v4_cidr_blocks = ["0.0.0.0/0"]
    from_port      = 1
    to_port        = 65535
  }

  ingress {
    protocol       = "ANY"
    description    = "Any"
    v4_cidr_blocks = ["0.0.0.0/0"]
    from_port      = 1
    to_port        = 65535
  }
}

resource "yandex_vpc_security_group" "vm-1-sg" {
  name       = "vm-1-sg"
  network_id = yandex_vpc_network.network1.id

  egress {
    protocol       = "ANY"
    description    = "any"
    v4_cidr_blocks = ["0.0.0.0/0"]
    from_port      = 1
    to_port        = 65535
  }

  ingress {
    protocol       = "TCP"
    description    = "ssh"
    v4_cidr_blocks = ["0.0.0.0/0"]
    port           = 22
  }

  ingress {
    protocol          = "TCP"
    description       = "victoria metrics"
    security_group_id = yandex_vpc_security_group.alb-sg.id
    port              = 8428
  }

  ingress {
    protocol          = "TCP"
    description       = "grafana"
    security_group_id = yandex_vpc_security_group.alb-sg.id
    port              = 3000
  }
}

resource "yandex_alb_backend_group" "backend_group" {
  for_each = local.alb_load_balancer.alb_backend_groups

  name = each.key

  dynamic "http_backend" {
    for_each = flatten([lookup(each.value, "http_backends", [])])
    content {
      name             = lookup(http_backend.value, "name", null)
      port             = lookup(http_backend.value, "port", null)
      target_group_ids = lookup(http_backend.value, "existing_target_groups_ids", null)

      dynamic "healthcheck" {
        for_each = flatten([lookup(http_backend.value, "healthcheck", [])])
        content {
          timeout          = lookup(healthcheck.value, "timeout", local.backend_defaults.healthcheck.timeout)
          interval         = lookup(healthcheck.value, "interval", local.backend_defaults.healthcheck.interval)
          healthcheck_port = lookup(healthcheck.value, "healthcheck_port", null)

          dynamic "http_healthcheck" {
            for_each = flatten([lookup(healthcheck.value, "http_healthcheck", [])])
            content {
              path = lookup(http_healthcheck.value, "path", local.backend_defaults.http_healthcheck.path)
            }
          }
        }
      }
    }
  }
}

resource "yandex_alb_http_router" "alb-router" {
  name = "alb-router"
}

resource "yandex_alb_virtual_host" "virtual_router" {
  for_each = local.alb_load_balancer.alb_virtual_hosts

  name           = each.key
  http_router_id = yandex_alb_http_router.alb-router.id
  authority      = lookup(each.value, "authority", [])

  dynamic "route" {
    for_each = flatten([lookup(each.value, "route", [])])

    content {
      name = lookup(route.value, "name", null)
      dynamic "http_route" {
        for_each = flatten([lookup(route.value, "http_route", [])])
        content {

          dynamic "http_match" {
            for_each = flatten([lookup(http_route.value, "http_match", [])])
            content {
              http_method = lookup(http_match.value, "http_method", null)
              dynamic "path" {
                for_each = flatten([lookup(http_match.value, "path", [])])
                content {
                  exact  = lookup(path.value, "exact", null)
                  prefix = lookup(path.value, "prefix", null)
                  regex  = lookup(path.value, "regex", null)
                }
              }
            }
          }

          dynamic "http_route_action" {
            for_each = flatten([lookup(http_route.value, "http_route_action", [])])
            content {
              backend_group_id = yandex_alb_backend_group.backend_group["${http_route_action.value.backend_group_name}"].id
              prefix_rewrite   = lookup(http_route_action.value, "prefix_rewrite", null)
            }
          }
        }
      }
    }
  }

  depends_on = [
    yandex_alb_backend_group.backend_group,
    yandex_alb_http_router.alb-router
  ]
}

resource "yandex_alb_load_balancer" "alb" {
  name               = "alb"
  network_id         = yandex_vpc_network.network1.id
  security_group_ids = [yandex_vpc_security_group.alb-sg.id]

  allocation_policy {
    location {
      zone_id   = "ru-central1-a"
      subnet_id = yandex_vpc_subnet.subnet-1.id
    }

    location {
      zone_id   = "ru-central1-b"
      subnet_id = yandex_vpc_subnet.subnet-2.id
    }

    location {
      zone_id   = "ru-central1-d"
      subnet_id = yandex_vpc_subnet.subnet-3.id
    }
  }

  listener {
    name = "alb-listener"
    endpoint {
      address {
        external_ipv4_address {
          # address = yandex_vpc_address.stat_address.external_ipv4_address[0].address
        }
      }
      ports = [80]
    }
    http {
      redirects {
        http_to_https = true
      }
      # handler {
      #   http_router_id = yandex_alb_http_router.alb-router.id
      # }
    }
  }

  listener {
    name = "listener-http"
    endpoint {
      address {
        external_ipv4_address {
          # address = yandex_vpc_address.stat_address.external_ipv4_address[0].address
        }
      }
      ports = [443]
    }
    tls {
      default_handler {
        http_handler {
          http_router_id = yandex_alb_http_router.alb-router.id
        }
        certificate_ids = [yandex_cm_certificate.lecerts[0].id]
      }
      sni_handler {
        name         = "mysite-sni"
        server_names = ["dev.${var.site_name}"]
        handler {
          http_handler {
            http_router_id = yandex_alb_http_router.alb-router.id
          }
          certificate_ids = [yandex_cm_certificate.lecerts[1].id]
        }
      }
      sni_handler {
        name         = "mysite-sni2"
        server_names = ["monitoring.${var.site_name}"]
        handler {
          http_handler {
            http_router_id = yandex_alb_http_router.alb-router.id
          }
          certificate_ids = [yandex_cm_certificate.lecerts[2].id]
        }
      }
      sni_handler {
        name         = "mysite-sni3"
        server_names = ["sonarqube.${var.site_name}"]
        handler {
          http_handler {
            http_router_id = yandex_alb_http_router.alb-router.id
          }
          certificate_ids = [yandex_cm_certificate.lecerts[3].id]
        }
      }
    }
  }
}

module "app_srvrs" {
  count  = 2
  source = "./modules/igroup"

  YC_KEYS     = var.YC_KEYS
  image_id    = var.appsrv_image_id
  ig-sg_id    = yandex_vpc_security_group.ig-sg.id
  network_id  = yandex_vpc_network.network1.id
  vpc_subnet1 = yandex_vpc_subnet.subnet-1.id
  vpc_subnet2 = yandex_vpc_subnet.subnet-2.id
  vpc_subnet3 = yandex_vpc_subnet.subnet-3.id
  group_name  = var.group_names[count.index]
  group_options = {
    one = {
      # cores          = 2,
      # memory         = 2,
      core_fraction  = 100,
      boot_disk_size = 10,
      zones          = ["ru-central1-a"],
      auto_scale = {
        initial_size           = 1
        measurement_duration   = 300
        stabilization_duration = 120
        cpu_utilization_target = 75
      }
    }
  }
}

module "consul" {
  count  = 1
  source = "./modules/igroup"

  YC_KEYS     = var.YC_KEYS
  image_id    = var.image_id
  ig-sg_id    = yandex_vpc_security_group.ig-sg.id
  network_id  = yandex_vpc_network.network1.id
  vpc_subnet1 = yandex_vpc_subnet.subnet-1.id
  vpc_subnet2 = yandex_vpc_subnet.subnet-2.id
  vpc_subnet3 = yandex_vpc_subnet.subnet-3.id
  group_name  = "consul"
  group_options = {
    one = {
      # cores          = 2,
      # memory         = 2,
      boot_disk_size = 10,
      zones          = ["ru-central1-a"],
      fixed_scale = {
        size = 3
      }
    }
  }
}

module "elastic" {
  count  = 1
  source = "./modules/igroup"

  YC_KEYS     = var.YC_KEYS
  image_id    = var.image_id
  ig-sg_id    = yandex_vpc_security_group.ig-sg.id
  network_id  = yandex_vpc_network.network1.id
  vpc_subnet1 = yandex_vpc_subnet.subnet-1.id
  vpc_subnet2 = yandex_vpc_subnet.subnet-2.id
  vpc_subnet3 = yandex_vpc_subnet.subnet-3.id
  group_name  = "elastic"
  group_options = {
    one = {
      # cores          = 2,
      memory         = 8,
      boot_disk_size = 10,
      zones          = ["ru-central1-a"],
      fixed_scale = {
        size = 3
      }
    }
  }
}

resource "yandex_alb_target_group" "monitoring" {
  name = "monitoring"

  target {
    subnet_id  = yandex_vpc_subnet.subnet-1.id
    ip_address = yandex_compute_instance.vm-1.network_interface.0.ip_address
  }
}

resource "yandex_alb_target_group" "sonarqube" {
  name = "sonarqube"

  target {
    subnet_id  = yandex_vpc_subnet.subnet-1.id
    ip_address = yandex_compute_instance.vm-2.network_interface.0.ip_address
  }
}

resource "yandex_compute_instance" "vm-1" {
  name        = "bastion"
  platform_id = "standard-v3"

  resources {
    core_fraction = 50
    cores         = 2
    memory        = 8
  }

  scheduling_policy {
    preemptible = true
  }

  boot_disk {
    mode = "READ_WRITE"
    initialize_params {
      image_id = var.adminwks_image_id
      size     = 15
    }
  }

  network_interface {
    subnet_id          = yandex_vpc_subnet.subnet-1.id
    nat                = true
    security_group_ids = [yandex_vpc_security_group.vm-1-sg.id]
  }

  labels = {
    group = "ci"
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_ed25519.pub")}"
  }
}

resource "yandex_compute_instance" "vm-2" {
  name        = "kibana"
  platform_id = "standard-v1"

  resources {
    core_fraction = 100
    cores         = 2
    memory        = 12
  }

  scheduling_policy {
    preemptible = true
  }

  boot_disk {
    mode = "READ_WRITE"
    initialize_params {
      image_id = var.adminwks_image_id
      size     = 15
    }
  }

  network_interface {
    subnet_id          = yandex_vpc_subnet.subnet-1.id
    # nat                = true
    security_group_ids = [yandex_vpc_security_group.ig-sg.id]
  }

  labels = {
    group = "elk"
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_ed25519.pub")}"
  }
}

# resource "yandex_compute_instance" "vm-3" {
#   name        = "sonarqube"
#   platform_id = "standard-v3"

#   resources {
#     core_fraction = 20
#     cores         = 2
#     memory        = 8
#   }

#   scheduling_policy {
#     preemptible = true
#   }

#   boot_disk {
#     mode = "READ_WRITE"
#     initialize_params {
#       image_id = var.adminwks_image_id
#       size     = 10
#     }
#   }

#   network_interface {
#     subnet_id          = yandex_vpc_subnet.subnet-1.id
#     # nat                = true
#     security_group_ids = [yandex_vpc_security_group.ig-sg.id]
#   }

#   labels = {
#     group = "sonarqube"
#   }

#   metadata = {
#     ssh-keys = "ubuntu:${file("~/.ssh/id_ed25519.pub")}"
#   }
# }
