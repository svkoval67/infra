#!/usr/bin/bash

# Сначала инициализируем файл состояния локально
# terraform init && terraform plan && terraform apply
# затем уже инициализируем backend
# ./init_backend.sh
# если удаленный файл состояния уже существует, то сразу запускаем ./init_backend.sh

source .keys.auto.tfvars
TF_ADDRESS="https://gitlab.com/api/v4/projects/$PROJECT_ID/terraform/state/$TF_STATE"

terraform init \
  -backend-config="address=${TF_ADDRESS}" \
  -backend-config="lock_address=${TF_ADDRESS}/lock" \
  -backend-config="unlock_address=${TF_ADDRESS}/lock" \
  -backend-config="username=${USER_BACKEND_LOGIN}" \
  -backend-config="password=${USER_BACKEND_PASS}" \
  -backend-config="lock_method=POST" \
  -backend-config="unlock_method=DELETE" \
  -backend-config="retry_wait_min=5"
