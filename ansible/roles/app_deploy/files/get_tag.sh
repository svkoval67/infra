#!/bin/bash
res=$(curl -s http://consul.tripleap.ru:8500/v1/kv/$ENV/release_version?raw)
( [[ -z $res ]] || [[ $res =~ "Moved Permanently" ]] ) && res=default
echo $res
