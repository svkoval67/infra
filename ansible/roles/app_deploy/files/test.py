import testinfra

def test_app_port(host):
    s = host.socket("tcp://0.0.0.0:8080")

    assert s.is_listening
