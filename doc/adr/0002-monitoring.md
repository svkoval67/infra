# 2. Monitoring

Date: 2023-04-28

## Status

Accepted

## Context

Наша система развивается и нам необходим мониторинг.

## Decision

Все говорят, что Prometheus + Grafana отличная связка, к тому же наше приложение умеет отдавать метрики в формате Prometheus.

## Consequences

Нет.
