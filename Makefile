include .env

.PHONY: all build_image build_infra run_playbook

all: build_infra run_playbook

build_image:
	@cd packer; \
		packer build -var token=$(yc iam create-token) -var folder_id=$(yc config get folder-id) -var api_token=${API_TOKEN} app_image.json

build_infra:
	@cd terraform; \
		terraform plan; \
		terraform apply -auto-approve

run_playbook:
	@cd ansible; \
		. venv/bin/activate; \
		ansible-playbook -b playbook.yaml
